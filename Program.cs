﻿using System;
using System.Net;
using System.IO;
using AngleSharp.Html.Parser;
using System.Text.RegularExpressions;

namespace OtusString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите URL-адрес: ");
            string path = Console.ReadLine();
            Console.WriteLine(Environment.NewLine+ "Результат:");

            //Regex regex = new Regex("[\"]https?://(.*?)[\"]");

            Regex regex = new Regex(@"(""|')https?://(.*?)(""|')");


            WebClient myClient = new WebClient();
            Stream stream = myClient.OpenRead(path);
            string text;
            using (StreamReader reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            MatchCollection matches = regex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }
        }
    }

}
